# OpenCore configuration for 'Ryzentosh'.

## Hardware

CPU: AMD Ryzen 9 3950X

CPU Cooler: Corsair H100i Pro RGB 240mm AIO

Motherboard: MSI MEG X570 ACE

Memory: 64Gb (4 x 16Gb) G.Skill Trident Z Neo 3600MHz DDR4

Graphics: Sapphire RX580 Nitro+ 8GB

Power Supply: SeaSonic 750W M12II EVO Modular Power Supply

Storage: ADATA SX8200Pro 1Tb NVMe PCIe SSD + ADATA SX8200Pro 512Gb NVMe PCIe SSD

Case: Corsair Carbide Series 275R (Tempered Glass Edition)

Other: Fenvi T-919 WiFi/Bluetooth
